import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import CreditCard from './Card.jsx'
import Alert from './Alert.jsx'

import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
    <CreditCard />
    <Alert />
  </React.StrictMode>,
)
